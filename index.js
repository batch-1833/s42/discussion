// console.log("Hello World");
// [SECTION] Document Object Model
// it allows us to access or modify the properties of an element in a webpage.
// it is a standard on how to get, change, add, or delete HTML elements.
/*
	Syntax: document.querySelector("htmlElement");

		- The querySelector function takes a string input that is formatted like a CSS Selector when applying styles.
*/

// Alternative way on retrieving HTML elements

// document.getElementById("txt-first-name");
// document.getElementByClassName("txt-last-name");

// However, using thes functions requires us to identify beforehand we get the element. With querySelector, we can be flexible in how to retrieve elements.

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
const txtColor = document.querySelector("#color");

//[SECTION] Event Listeners
// Whenever a user interacts with a web page, this action is considered as an event. (example: mouse click, mouse hover, page load, key press, etc.)

// addEventListener takes two arguments:
	// a string that identify an event.
	// a function that the listener will execute once the "specified event" is triggered.
// txtFirstName.addEventListener("keyup", () =>{
// 	// ".innerHTML" prorperty sets or returns the HTML content(inner HTML) of an element (div, spans, etc.).
// 	// ".value" property sets or returns the value of an attribute (form controls).
// 	spanFullName.innerHTML = `${txtFirstName.value}`;
// });

// When the event occurs, an "event object" is passed to the function argument as the first parameter.
// txtFirstName.addEventListener("keyup", (event) =>{
// 	//The "event.target" contains the element where the event happened.
// 	console.log(event.target);
// 	// The "event.target.value" gets the value of the input object(txt-first-name)
// 	console.log(event.target.value);
// })

// Creating Multiple event that uses the same function.
// const fullName = () =>{
// 	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
// }

// txtFirstName.addEventListener("keyup", fullName);
// txtLastName.addEventListener("keyup", fullName);

const fullName = () =>{
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

txtColor.addEventListener("change", ()=> {
	spanFullName.style.color = txtColor.value;
})
